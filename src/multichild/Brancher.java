/*
 * Custom branch callback creating 3+ children.
 *
 * When branching at the root node, this will create one child node for
 * each possible count on the number of teams. At other nodes, it will
 * let CPLEX branch as usual.
 */
package multichild;

import ilog.concert.IloException;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author Paul A. Rubin (http://about.me/paul.a.rubin)
 */
public class Brancher extends IloCplex.BranchCallback {
  private int minTeams;  // minimum number of teams to create
  private int maxTeams;  // maximum number of teams to create
  private IloNumVar[] teamVars;  // boolean variables for team use
  private boolean atRoot;  // are we at the root node?
  private ArrayList<Double> bounds; // bounds for variables when branching
  private ArrayList<IloCplex.BranchDirection> dirs; // branch directions
  private ArrayList<IloNumVar> vars;  // variables involved in branching
  private double[] aBds;  // bounds as doubles
  private IloCplex.BranchDirection[] aDirs;  // directions as array
  private IloNumVar[] aVars;  // variables as array

  /**
   * Constructor.
   * @param min  minimum number of teams to create
   * @param max  maximum number of teams to create
   * @param v         boolean variables for team use
   * @throws Exception if the variable array has the wrong dimension
   */
  public Brancher(final int min, final int max, final IloNumVar[] v)
         throws Exception {
    super();
    this.minTeams = min;
    this.maxTeams = max;
    this.teamVars = Arrays.copyOf(v, v.length);
    // make sure the variable vector has the correct dimension
    if (v.length != max) {
      throw new Exception("Variable array dimension " + v.length
                          + " does not match maxTeams = " + max);
    }
    this.atRoot = true;
  }

  /**
   * The actual callback operation.
   * @throws IloException if CPLEX gets miffed
   */
  @Override
  protected final void main() throws IloException {
    BranchInfo info = null;
    Object raw;
    String msg = ">>> At node " + getNodeId() + ", ";
    IloCplex.NodeId id;
    // check whether we are at the root node (which will have no attachment)
    if (atRoot) {
      raw = new BranchInfo(minTeams, maxTeams);  // create an attachment
      atRoot = false;  // clear the root flag
    } else {
      raw = getNodeData();  // get the current node's attachment, if any
    }
    if (raw == null) {
      // no data object -- let CPLEX handle branching
      return;
    } else if (raw instanceof BranchInfo) {
      // convert the object to an instance of BranchInfo
      info = (BranchInfo) raw;
    } else {
      // unknown node data type -- should never happen
      System.err.println("Encountered unknown node data type!");
      abort();
    }
    // the presence of node data means we are branching on team count
    // if the range of team sizes at this node is one or two, just create
    // normal children; otherwise, bisect the range of counts and create
    // two "special" children
    int min = info.getMinTeams();
    int max = info.getMaxTeams();
    msg += "team range is " + min + " to " + max;
    double obj = getObjValue();  // current node bound
    switch (max - min) {
      case 0: // create a single child with exactly this many teams
        // this case should only occur (at the outset) if minTeams = maxTeams
        cut(min, max);
        id = makeBranch(aVars, aBds, aDirs, obj); // attach no data
        msg += " -- adding one normal child ("
               + id + ") with exactly " + min + " teams";
        System.out.println(msg);
        return;
      case 1: // create two normal children
        // first child contains exactly min teams, no attached data
        cut(min, min);
        id = makeBranch(aVars, aBds, aDirs, obj); // attach no data
        msg += " -- adding two normal children (" + id + " using exactly "
               + min + " teams and ";
        // second child contains exactly max = min + 1 teams, no attachment
        cut(max, max);
        id = makeBranch(aVars, aBds, aDirs, obj); // attach no data
        msg += id + " using exactly " + max + " teams)";
        System.out.println(msg);
        return;
      case 2: // create one normal child and one composite child
        // first child uses min to min+1 teams
        cut(min, min + 1);
        // attach node info to this child
        info = new BranchInfo(min, min + 1);
        id = makeBranch(aVars, aBds, aDirs, obj, info);
        msg += " -- adding one composite child (" + id + ") with range "
               + min + " to " + (min + 1);
        // second child uses exactly max = min + 2 teams, no attached data
        cut(max, max);
        id = makeBranch(aVars, aBds, aDirs, obj);
        msg += " and one normal child (" + id + ") using exactly "
               + max + " teams";
        System.out.println(msg);
        return;
      default: // max > min + 2: create two composite children
        int mid = (max + min) / 2;  // midpoint of size range (rounding down)
        // first child uses min to mid team count
        cut(min, mid);
        info = new BranchInfo(min, mid);
        id = makeBranch(aVars, aBds, aDirs, obj, info);
        msg += " -- adding two composite children (" + id
               + " with range " + min + " to " + mid;
        // second child uses mid + 1 to max team count
        cut(mid + 1, max);
        info = new BranchInfo(mid + 1, max);
        id = makeBranch(aVars, aBds, aDirs, obj, info);
        msg += " and " + id + " with range " + (mid + 1) + " to " + max + ")";
        System.out.println(msg);
    }
  }

  /**
   * Sets up the arguments for a cut that ensures somewhere between min and
   * max teams are used.
   * @param min minimum number of teams to use
   * @param max maximum number of teams to use
   */
  private void cut(final int min, final int max) {
    this.bounds = new ArrayList<Double>();
    this.dirs = new ArrayList<IloCplex.BranchDirection>();
    this.vars = new ArrayList<IloNumVar>();
    for (int j = 0; j < maxTeams; j++) {
      if (j < min) {  // force the first min variables to be 1
        vars.add(teamVars[j]);
        bounds.add(1.0);
        dirs.add(IloCplex.BranchDirection.Up);
      } else if (j >= max) {  // force variables above max to be 0
        vars.add(teamVars[j]);
        bounds.add(0.0);
        dirs.add(IloCplex.BranchDirection.Down);
      }
    }
    // convert the lists to arrays
    aVars = vars.toArray(new IloNumVar[0]);
    aDirs = dirs.toArray(new IloCplex.BranchDirection[0]);
    // unavoidable PITA -- bounds must be converted from Double[] to double[]
    // with a loop
    Double[] temp = bounds.toArray(new Double[0]);
    aBds = new double[temp.length];
    for (int i = 0; i < temp.length; i++) {
      aBds[i] = temp[i];
    }
  }

}

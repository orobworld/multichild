/*
 * Example of how to generate > 2 child nodes in CPLEX.
 * Context: assigning students to teams to balance average GPA.
 * Team sizes are constrained, and the head counts of the largest and smallest
 * teams should differ by at most one.
 */
package multichild;

import ilog.concert.IloException;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;
import java.util.Random;

/**
 * @author Paul A. Rubin (http://about.me/paul.a.rubin)
 */
public final class MultiChild {

  /**
   * Constructor (which does nothing).
   */
  private MultiChild() { }

  /**
   * Main program.
   * @param args command line arguments (ignored)
   */
  @SuppressWarnings({ "checkstyle:magicnumber" })
  public static void main(String[] args) {
    // problem parameters
    int nStudents = 60;  // number of students to form into teams
    int minSize = 4;     // minimum team size
    int maxSize = 6;     // maximum team size
    int maxTeams = (int) Math.floor((1.0 * nStudents) / minSize);
      // maximum number of teams
    int minTeams = (int) Math.ceil((1.0 * nStudents) / maxSize);
      // minimum number of teams
    double[] gpa = new double[nStudents];
    // generate random GPAs
    Random gen = new Random(666L);
    for (int i = 0; i < nStudents; i++) {
      gpa[i] = Math.rint(200.0 * (1.0 + gen.nextDouble())) / 100;
        // using Math.rint to get GPAs with exactly two decimal places
    }
    /*
     * The MIP model will look like the following, where
     *   x[i][j] = 1 if student i is assigned to team j, else 0
     *   y[j] = 1 if team j is used, else 0
     *   s[j][k] = 1 if team j has size k (k = minSize, ..., maxSize), else 0
     *   gmax = highest composite GPA of any team
     *   gmin = lowest composite GPA of any team
     *   gsum[j] = sum of GPAs of students on team j
     *
     * minimize gmax - gmin
     * s.t.
     *   sum_j x[i][j] = 1 for all i (assign every student once)
     *   sum_k k*s[j][k] = sum_i x[i][j] for all j (size of each team)
     *   sum_k s[j][k] = y[j] for all j  (size is SOS1; unused teams have
     *     size 0)
     *   y[j] <= y[j - 1] for all j > 0 (unused teams come at end of list)
     *   gsum[j] = sum_i gpa[i]*x[i][j] for all j (GPA sums)
     *   gsum[j] <= k*gmax + 4*maxSize*(1 - s[j][k]) for all j, k (defines gmax)
     *   gsum[j] >= k*gmin - 4*k*(1 - s[j][k]) for all j, k (defines gmin)
     */
    try {
      IloCplex cplex = new IloCplex();
      // create the variables
      IloNumVar[][] x = new IloNumVar[nStudents][maxTeams];
      IloNumVar[] y = new IloNumVar[maxTeams];
      IloNumVar[][] s = new IloNumVar[maxTeams][maxSize];
      IloNumVar[] gsum = new IloNumVar[maxTeams];
      IloNumVar gmax = cplex.numVar(0.0, 4.0, "gmax");
      IloNumVar gmin = cplex.numVar(0.0, 4.0, "gmin");
      for (int i = 0; i < nStudents; i++) {
        for (int j = 0; j < maxTeams; j++) {
          x[i][j] = cplex.boolVar("x_" + i + "_" + j);
        }
      }
      for (int j = 0; j < maxTeams; j++) {
        y[j] = cplex.boolVar("y_" + j);
        gsum[j] = cplex.numVar(0.0, Double.MAX_VALUE, "gsum_" + j);
        for (int k = minSize - 1; k < maxSize; k++) {
          s[j][k] = cplex.boolVar("s_" + j + "_" + (k + 1));
        }
      }
      // add the objective function
      cplex.addMinimize(cplex.diff(gmax, gmin));
      // add the constraints
      for (int i = 0; i < nStudents; i++) {
        cplex.addEq(cplex.sum(x[i]), 1.0); // single assignment, student i
      }
      for (int j = 0; j < maxTeams; j++) {
        IloLinearNumExpr expr1 = cplex.linearNumExpr();
        IloLinearNumExpr expr2 = cplex.linearNumExpr();
        for (int k = minSize - 1; k < maxSize; k++) {
          expr1.addTerm(s[j][k], k + 1);
        }
        for (int i = 0; i < nStudents; i++) {
          expr2.addTerm(x[i][j], 1.0);
        }
        cplex.addEq(expr1, expr2); // size definition for team j
        expr1.clear();
        for (int k = minSize - 1; k < maxSize; k++) {
          expr1.addTerm(s[j][k], 1.0);
        }
        cplex.addEq(expr1, y[j]); // size is SOS1; unused teams have size 0
        if (j > 0) {
          cplex.addLe(y[j], y[j - 1]);  // used teams come first
        }
        expr1.clear();
        for (int i = 0; i < nStudents; i++) {
          expr1.addTerm(x[i][j], gpa[i]);
        }
        cplex.addEq(expr1, gsum[j]);  // define GPA sums
        for (int k = minSize - 1; k < maxSize; k++) {
          int kk = k + 1;
          expr1.clear();
          expr2.clear();
          expr1.setConstant(4 * maxSize);
          expr2.setConstant(-4 * kk);
          expr1.addTerm(gmax, kk);
          expr2.addTerm(gmin, kk);
          expr1.addTerm(s[j][k], -4 * maxSize);
          expr2.addTerm(s[j][k], 4 * kk);
          cplex.addLe(gsum[j], expr1); // upper bound on GPA sum
          cplex.addGe(gsum[j], expr2); // lower bound on GPA sum
        }
      }
      // set a tolerance so that we don't split hairs
      cplex.setParam(IloCplex.DoubleParam.EpAGap, 0.01);
      // attach a branch callback -- comment this out to run without
      // special branching
      cplex.use(new Brancher(minTeams, maxTeams, y));
      // solve the model
      cplex.solve();
      if (cplex.getStatus() == IloCplex.Status.Optimal) {
        // fetch the solution
        double[][] xx = new double[nStudents][];
        for (int i = 0; i < nStudents; i++) {
          xx[i] = cplex.getValues(x[i]);
        }
        double[] yy = cplex.getValues(y);
        double[] ggsum = cplex.getValues(gsum);
        // display the optimal solution
        System.out.println("\nAssign " + nStudents + " students into teams "
                           + "of size " + minSize + " to " + maxSize + ".");
        System.out.println("GPA spread is from " + cplex.getValue(gmin)
                           + " to " + cplex.getValue(gmax)
                           + " (a spread of " + cplex.getObjValue() + ")");
        System.out.println("CPLEX explored " + cplex.getNnodes() + " nodes.");
        System.out.println("\nTeam\tStudent\tGPA");
        for (int j = 0; j < maxTeams; j++) {
          if (yy[j] > 0.5) {
            int size = 0;
            for (int i = 0; i < nStudents; i++) {
              if (xx[i][j] > 0.5) {
                size++;
                System.out.println(j + "\t" + i + "\t" + gpa[i]);
              }
            }
            System.out.println("\tOVERALL\t" + ggsum[j] / size + "\n");
          }
        }
      } else {
        System.err.println("CPLEX returned status " + cplex.getStatus());
        System.exit(1);
      }
    } catch (IloException ex) {
      System.err.println("CPLEX pitched a hissy fit:\n" + ex.getMessage());
      System.exit(1);
    } catch (Exception ex) {
      System.err.println("Could not construct the callback!\n"
                         + ex.getMessage());
      System.exit(1);
    }
  }
}
